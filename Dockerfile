FROM node:14-alpine
LABEL application="shinobi-msx"
LABEL maintainer="Oleg Vivtash <o@vivtash.net>"
LABEL version="1.1.0"

#ENV SHINOBI_URL
#ENV SHINOBI_API_KEY
#ENV SHINOBI_GROUP
#ENV EXTERNAL_URL
ENV PORT=3000

WORKDIR /opt/shinobi-msx

COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 3000
CMD [ "node", "index.js" ]


