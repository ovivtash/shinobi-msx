# Shinobi-MSX

> The way to bring your IP cameras to the TVs of all kinds and sizes!

Shinobi-MSX is a small server application that aggregates IP cameras from your [Shinobi](https://shinobi.video/) instance
and provides them as a content for [Media Station X](https://msx.benzac.de/info).
This way, you can explore live streams (and recordings in the future) on any TV or tablet that is capable of running 
Media Station X.

![Shinobi MSX Screenshot](shinobi-msx.png "Screenshot")

Tested on Android TV, Tizen (Samsung TV) and webOS (LG TV). Please refer to 
[Platform Support](https://msx.benzac.de/info/?tab=PlatformSupport) page to check your TV or device compatibility.

Since it's mostly about TVs, Shibobi-MSX leverages Shinobi's "TV Channel" capability. This way you can choose which
cameras will be shown in the portal. If you don't see any or particular camera in Shinobi-MSX, please make sure the following
settings are set in "Stream" section of Shinobi camera configuration:
* `TV Channel` - should be set to "Yes"
* `TV Channel ID` - this is how a camera will be named. This can be different from regular camera name.
* `TV Channel Group` - a camera group name. Shinobi-MSX uses this value to group cameras in the menu. Also there is an
option to see a single list of all cameras and "ungrouped" list of cameras for which this setting is left empty. 

### Security notice

Usually Shinobi-MSX should be ran on the same LAN where your Shinobi server is accessible. You can expose Shinobi-MSX
to the Internet, but this is strongly  discouraged until you **understand the consequences** as there is 
**no authentication** or other kind of protection (DoS, etc.) and **anyone on the planet will be able to access 
camera streams**. Also, your Shinobi API key and Group ID are passed unencrypted in Shinobi-MSX HTTP responses.
So it's better to keep this stuff local.

### Configuration

Configuration is performed via environment variables listed below.

* `SHINOBI_URL` - A full root URL to your Shinobi instance, i.e. `http://shinobi.local:8080` or `http://192.168.0.16:8080`
* `SHINOBI_API_KEY` - Shinobi user API key. Can be obtained in Shinobi web UI under "API" dialog available under user menu dropdown. 
* `SHINOBI_GROUP` - Camera group key. Shown in the header of "API" dialog, i.e `RF3d78X`
* `EXTERNAL_URL` - A full URL where Shinobi-MSX will be available in your local network. **Same value should be set as MSX Start Parameter.**
* `PORT` - HTTP port number where Shinobi-MSX server should listed to requests. _Default is 3000._

### Docker installation ![Docker Image Version (latest semver)](https://img.shields.io/docker/v/duhast/shinobi-msx?label=docker%20image)
Docker image is available from [Docker Hub](https://hub.docker.com/r/duhast/shinobi-msx). HTTP port is predefined in the image to 3000, so there is no need to configure it. 
Below is an example of running a Shinobi-MSX in background where HTTP port is mapped to 4111 on the host system. API key and group key are fictional, so replace them with yours.

```bash
docker run -d --name shinobi-msx -p 4111:3000 \
  -e SHINOBI_URL=http://192.168.0.16:8080 \
  -e SHINOBI_API_KEY=XAL8sjxqrh778z3nVUWdkdrH \
  -e SHINOBI_GROUP=s4Fn5RRM \
  -e EXTERNAL_URL=http://192.168.0.10:4111 \
  duhast/shinobi-msx
```

### Regular installation

1. Make sure NodeJS v14 or higher is installed
2. Clone the repo to the folder of your choice 
3. Run `npm install`
4. Run `npm start` with ENV variables properly set to verify everything works
5. For running as a service, use [PM2](https://pm2.keymetrics.io/) ot other method you'd like
