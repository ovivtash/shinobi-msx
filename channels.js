import {config} from "./config.js";
import _ from "lodash";
import {compactJoin, msxUrl} from "./utils.js";
import {shinobi} from "./data.js";

export function channelRoutes(fastify, options, done) {

    fastify.get('/channels.json', async (req, res) => {
        const channels = await shinobi.getTvChannels(config.shinobi.groupKey)
        if (req.query.ungrouped) {
            _.remove(channels, chan => chan.groupTitle !== '')
        } else if (req.query.group) {
            _.remove(channels, chan => chan.groupTitle !== req.query.group)
        }

        if(channels.length === 0){
            return {
                type: 'pages',
                title: 'No Streams',
                pages: [
                    {
                        action: 'warn:No streams found for this section. Please verify Shinoby TV channel setup and Group Key.',
                        items: [{
                            layout: '0,0,12,3',
                            text: 'No streams found for this section'
                        }]
                    }
                ]

            }
        }

        return {
            type: "list",
            headline: "Live Streams",
            template: {
                layout: '0,0,4,3',
                imageFiller: 'width-center',
                type: "separate",
                icon: "msx-white-soft:live-tv",
                color: "msx-glass"
            },
            items: _.map(channels, chan => {
                let action = `video:${shinobi.getFullUrl(_.first(chan.streams))}`
                if(_.size(chan.streams) === 0){
                   action = 'warn:This camera has no streams'
                }
                return {
                    playerLabel: compactJoin([chan.groupTitle, chan.channel], ': '),
                    tag: chan.type,
                    title: chan.channel,
                    image: shinobi.getFullUrl(chan.snapshot),
                    action
                }
            })
        }
    })

    done()
}