import {promises as fs} from 'node:fs'

const pkgInfo = JSON.parse(await fs.readFile('package.json'))

export const version = pkgInfo.version

export const config = {
    shinobi: {
        url: process.env.SHINOBI_URL,
        apiKey: process.env.SHINOBI_API_KEY,
        groupKey: process.env.SHINOBI_GROUP,
    },
    external_url: process.env.EXTERNAL_URL,
    listen_ip: process.env.LISTEN_IP || '0.0.0.0',
    port: process.env.PORT || 3000,
}

if(!config.external_url) throw new Error('External URL where Shinobi MSX is accessible is not defined')
if(!config.shinobi.url) throw new Error('Shinobi URL is not defined')
if(!config.shinobi.apiKey) throw new Error('Shinobi API key is not defined')
if(!config.shinobi.groupKey) throw new Error('Shinobi Group Key is not defined')

