import {URL, URLSearchParams} from "url"
import {config} from "./config.js"
import path from 'path'
import { fileURLToPath } from 'url'
import _ from "lodash"

const __filename = fileURLToPath(import.meta.url)
export const _dirname = path.dirname(__filename)

export const msxUrl = (path, params) => {
    const url = new URL(path, config.external_url)
    if(_.isObject(params)){
        url.search = new URLSearchParams(params)
    }
    return url.href;
}


export const compactJoin = (stringArray, separator) => {
    return _.filter(stringArray, e => e && _.isString(e) && e.length > 0).join(separator)
}

export const composeErrorPage = (error) => {
    return {
        type: 'pages',
        title: error.name,
        pages: [
            {
                action: `error:${error.message}`,
                items: [{
                    layout: '0,0,12,3',
                    text: `${error.name}{br}${error.message}`
                }]
            }
        ]
    }
}
