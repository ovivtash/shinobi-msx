import fetch, {AbortError} from "node-fetch"
import AbortController from "abort-controller"

class ShinobiError extends Error {
    constructor(message) {
        super(message)
        this.name = 'ShinobiError'
    }
}

export class ShinobiApi {

    constructor(rootUrl, apiKey) {
        this.rootUrl = rootUrl
        this.apiKey = apiKey
    }

    getFullUrl(path) {
        return `${this.rootUrl}${path}`
    }

    async request(endpoint) {
        const controller = new AbortController()
        const timeout = setTimeout(() => controller.abort(), 5000)

        const requestOpts = {
            headers: {'Content-Type': 'application/json'},
            signal: controller.signal
        }
        try {
            const response = await fetch(`${this.rootUrl}/${this.apiKey}/${endpoint}/`, requestOpts)
            const data = await response.json()
            return data
        }
        catch (error) {
            if (error instanceof AbortError) {
                throw new ShinobiError('Request timeout. Please verify your API key.')
            } else {
                throw error
            }
        } finally {
            clearTimeout(timeout);
        }
    }

    async getTvChannels(groupKey) {
        const channels = await this.request(`tvChannels/${groupKey}`)
        return channels;
    }



}