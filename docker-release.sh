#!/bin/bash

VERSION=$(jq -r .version package.json)

echo Building version $VERSION ...
sed -i '/LABEL version=/c\LABEL version="'"$VERSION"'"' ./Dockerfile

docker build --tag duhast/shinobi-msx:latest --tag duhast/shinobi-msx:$VERSION .

read -r -p "Push to Docker Hub now? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
  docker push duhast/shinobi-msx:latest
  docker push duhast/shinobi-msx:$VERSION
fi